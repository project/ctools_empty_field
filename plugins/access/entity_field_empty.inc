<?php

/**
 * @file
 * Plugin to provide access control/visibility based on a field being empty.
 */

$plugin = array(
  'title'         => t('Field is empty'),
  'description'   => t('Determines whether a field is empty.'),
  'callback'      => 'ctools_empty_field_check',
  'settings form' => 'ctools_empty_field_settings',
  'summary'       => 'ctools_empty_field_summary',
  'defaults'      => array(
    'field'       => '',
    'entity_type' => '',
    'bundle'      => '',
  ),
  'get child'     => 'ctools_empty_field_get_child',
  'get children'  => 'ctools_empty_field_get_children',
);

/**
 * Plugin callback: "get child".
 */
function ctools_empty_field_get_child($plugin, $parent, $child) {
  $plugins = &drupal_static(__FUNCTION__, array());
  if (empty($plugins[$parent . ':' . $child])) {
    list($entity_type, $bundle_type) = explode(':', $child);
    $plugins[$parent . ':' . $child]
      = _ctools_empty_field_get_child(
        $plugin, $parent, $entity_type, $bundle_type);
  }

  return $plugins[$parent . ':' . $child];
}

/**
 * Plugin callback: "get children".
 *
 * Returns separate plugins for each bundle type of every entity type.
 */
function ctools_empty_field_get_children($plugin, $parent) {
  $plugins = &drupal_static(__FUNCTION__, array());
  if (!empty($plugins)) {
    return $plugins;
  }
  $entities = entity_get_info();
  foreach ($entities as $entity_type => $entity) {
    foreach ($entity['bundles'] as $bundle_type => $bundle) {
      if (!isset($plugins[$parent . ':' . $entity_type . ':' . $bundle_type])) {
        $plugin = _ctools_empty_field_get_child(
          $plugin, $parent, $entity_type, $bundle_type, $entity, $bundle);
        $plugins[$parent . ':' . $entity_type . ':' . $bundle_type] = $plugin;
      }
    }
  }

  return $plugins;
}

/**
 * Internal function for loading child plugins.
 */
function _ctools_empty_field_get_child($plugin, $parent, $entity_type, $bundle_type, $entity = NULL, $bundle = NULL) {
  // Check that the entity and bundle arrays have data. If not, load them.
  if (empty($entity)) {
    $entity = entity_get_info($entity_type);
  }
  if (empty($bundle)) {
    $bundle = $entity['bundles'][$bundle_type];
  }

  return array(
    'title' => t('Field is empty: @entity: @bundle', array(
      '@entity' => $entity['label'],
      '@bundle' => $bundle['label'],
    )),
    'keyword'          => $entity_type,
    'description'      => t('Determines whether a field is empty.'),
    'name'             => $parent . ':' . $entity_type . ':' . $bundle_type,
    'required context' => new ctools_context_required(t(ucfirst($entity_type)), $entity_type, array(
      'type' => $bundle_type,
    )),
  ) + $plugin;
}

/**
 * Plugin callback for the settings form.
 */
function ctools_empty_field_settings($form, &$form_state, $conf) {
  // Add entity type and bundle to the settings data.
  list(, $entity_type, $bundle) = explode(':', $form_state['plugin']['name']);
  $form['settings']['entity_type'] = array(
    '#type'  => 'value',
    '#value' => $entity_type,
  );
  $form['settings']['bundle'] = array(
    '#type'  => 'value',
    '#value' => $bundle,
  );

  // Add the "field" setting. Generate and sort the options list.
  $wrapper = _ctools_empty_field_wrapper(array(
    'entity_type' => $entity_type,
    'bundle'      => $bundle,
  ));
  foreach ($wrapper->getPropertyInfo() as $property => $info) {
    $options[$property] = $info['label'] . " ($property)";
  }
  asort($options);
  $form['settings']['field'] = array(
    '#type'          => 'select',
    '#title'         => t('Field'),
    '#default_value' => $conf['field'],
    '#required'      => TRUE,
    '#options'       => $options,
  );
  return $form;
}

/**
 * Checks for access.
 *
 * @return bool
 *   TRUE if field is missing or empty; FALSE otherwise.
 *
 * @see https://secure.php.net/empty
 */
function ctools_empty_field_check($conf, $context) {
  $entity = _ctools_empty_field_wrapper($conf, $context);
  $field  = $conf['field'];
  try {
    if (empty($entity->$field)) {
      return TRUE;
    }
    $value = $entity->$field->value();
  }
  // If the field does not exist on this hydrated entity, EMW throws up, which
  // is fine!
  catch (EntityMetadataWrapperException $e) {
    return TRUE;
  }
  return empty($value);
}

/**
 * Provides a summary description.
 */
function ctools_empty_field_summary($conf, $context) {
  $wrapper = _ctools_empty_field_wrapper($conf, $context);
  $field   = $conf['field'];
  $info    = $wrapper->$field->info();
  return $info['label'] . ' ' . t('is empty');
}

/**
 * Gets a wrapper for the appropriate entity type and bundle.
 */
function _ctools_empty_field_wrapper($conf, $context = NULL) {
  $wrapper = entity_metadata_wrapper(
    $conf['entity_type'],
    empty($context->data) ? NULL : $context->data,
    array('bundle' => $conf['bundle'])
  );
  return $wrapper;
}
