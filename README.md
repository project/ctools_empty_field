# CTools Empty Field Access Plugin

## Project Description

The "entity_field_value" plugin rarely suffices for testing empty values. Use this plugin when you need pane visibility rules or variant selection criteria that test for a field (or property) value being empty.

## Future Enhancements

-   ~~Create a README.~~
-   Change the plugin titles so they make more sense in the Access Plugin list.
-   Allow configuration of entity types / bundles to allow, to keep the Access
    Plugin list from exploding.
-   Port to D8 (?).
